/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.stack;

/**
 *
 * @author informatics
 */
class Stack {
    public static void main(String[] args) {
        StackX theStack = new StackX(10);   
        theStack.push('a');                  
        theStack.push('b');
        theStack.push('c');
        theStack.push('d');
        
        while (!theStack.isEmpty()) {       
            char value = theStack.pop();    
            System.out.print(value);        
            System.out.print(" ");
        }
        System.out.println("\nNot Empty Stack");
        
        if(theStack.isEmpty()) System.out.println(" Empty");
    }
}